# This file contains basically only integration tests.

import unittest
import json

from src.structure_parser.structure_parser import StructureParser
from src.dependency_injector import DependencyInjector
from src.function_parser import FunctionParser
from src.path_parser import PathParser
from src.cluster_parser import ClusterParser


class PathParserTest(unittest.TestCase):
    template = json.loads("""
    {
        "name": "_.data.name"
    }
    """)

    input = json.loads("""
    {
        "data": {
            "name": "TestName",
            "name2": "TestName2"
        }
    }
    """)

    required_output = json.loads("""
        {
            "name": "TestName"
        }
    """)

    dependency_injection = DependencyInjector(PathParser, FunctionParser, StructureParser, {"$cluster": ClusterParser})
    parser = StructureParser(dependency_injection)

    def test_parse_structure_dict_and_value_path(self):
        test_output = self.parser.parse(self.input, self.template)
        self.assertEqual(self.required_output, test_output)

    def test_parse_structure_dict_and_key_path(self):
        test_required_output = json.loads("""
        {
            "TestName2": "name"
        }
        """)
        test_template = json.loads("""
        {
            "_.data.name2": "name"
        }

        """)

        test_output = self.parser.parse(self.input, test_template)
        self.assertEqual(test_output, test_required_output)

    def test_parse_structure_flatten_key_value(self):
        test_required_output = json.loads("""
        {
            "TestName": "13",
            "TestName2": "16"
        }
        """)
        test_template = json.loads("""
        {
            "__.data.name": "__.data.age"
        }

        """)

        test_input = json.loads("""
        { "data":
            [
                {"name": "TestName","age": "13"},
                {"name": "TestName2","age": "16"}
            ]
        }""")

        test_output = self.parser.parse(test_input, test_template)
        self.assertEqual(test_output, test_required_output)

    def test_parse_structure_list_and_list(self):
        test_required_output = json.loads("""
        {"newData": [
            {"name": ["TestName"]},
            {"name2": ["TestName2"]}
        ]}
        """)
        test_template = json.loads("""
        {
            "newData": [
                {"name": "_.data.name"},
                {"name2": "_.data.name2"}
            ]
        }

        """)

        test_input = json.loads("""
        {
            "data": [
                {"name": "TestName"},
                {"name2": "TestName2"}
            ]
        }
        """)

        test_output = self.parser.parse(test_input, test_template)
        self.assertEqual(test_output, test_required_output)

    def test_parse_structure_list(self):
        test_required_output = json.loads("""
        {
            "newData": [
                {"name": "TestName"},
                {"name2": "TestName2"}
            ]
        }
        """)
        test_template = json.loads("""
        {
            "newData": [
                {"name": "_.data.name"},
                {"name2": "_.data.name2"}
            ]
        }

        """)

        test_input = json.loads("""
        {
            "data": {
                "name": "TestName",
                "name2": "TestName2"
            }
        }
        """)

        test_output = self.parser.parse(test_input, test_template)
        self.assertEqual(test_output, test_required_output)

    def test_parse_function(self):
        test_required_output = json.loads("""
        [
            {"testname": "TestName","testage": "13"},
            {"testname": "TestName2","testage": "16"}
        ]""")
        test_template = json.loads("""
        {
            "$cluster": {
                "_.data.rows": {
                    "testname": "_.data.rows.name",
                    "testage": "_.data.rows.age"
                }
            }
        }
        """)

        test_input = json.loads("""
        {
            "data": {
                "rows": [
                    {"name": "TestName", "age":"13"},
                    {"name": "TestName2", "age":"16"}
                ]
            }
        }
        """)

        test_output = self.parser.parse(test_input, test_template)
        self.assertEqual(test_output, test_required_output)