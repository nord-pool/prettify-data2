import json
import unittest

from src.json_to_json_converter import JsonToJsonConverter

input = json.loads("""
{
  "data": {
    "Rows": [
      {
        "Columns": [
          {
            "Name": "Product",
            "Value": "PH-20151231-01"
          },
          {
            "Name": "High",
            "Value": "14,00"
          },
          {
            "Name": "Low",
            "Value": "11,30"
          },
          {
            "Name": "Last",
            "Value": "11,30"
          },
          {
            "Name": "Avg",
            "Value": "13,33"
          },
          {
            "Name": "Volume",
            "Value": "40,00"
          }
        ],
        "Name": "PH-20151231-01"
      },
      {
        "Columns": [
          {
            "Name": "Product",
            "Value": "PH-20151231-02"
          },
          {
            "Name": "High",
            "Value": "13,00"
          },
          {
            "Name": "Low",
            "Value": "11,10"
          },
          {
            "Name": "Last",
            "Value": "13,00"
          },
          {
            "Name": "Avg",
            "Value": "12,71"
          },
          {
            "Name": "Volume",
            "Value": "65,00"
          }
        ],
        "Name": "PH-20151231-02"
      }
    ],
    "DataStartdate": "2014-11-25T00:00:00",
    "DataEnddate": "2016-01-01T08:00:00Z"
  },
  "endDate": "31-12-2015"
}
""")

template = json.loads("""
{
    "Date": "_.endDate",
    "Hours":
        {
            "$cluster": {
                "_.data.Rows": {
                    "Id": "_.data.Rows.Name",
                    "__.data.Rows.Columns.Name": "__.data.Rows.Columns.Value"
                }
            }
        }
}
""")

output = json.loads("""{
    "Date": "31-12-2015",
    "Hours": [
        {
            "Id": "PH-20151231-01",
            "High": "14,00",
            "Low": "11,30",
            "Last":"11,30",
            "Avg": "13,33",
            "Volume": "40,00",
            "Product": "PH-20151231-01"
        },
        {
            "Id": "PH-20151231-02",
            "High": "13,00",
            "Low": "11,10",
            "Last":"13,00",
            "Avg": "12,71",
            "Volume": "65,00",
            "Product": "PH-20151231-02"
        }
    ]
}""")


class JsonToJsonConverterTest(unittest.TestCase):
    def testConvert(self):
        converter = JsonToJsonConverter()
        test_output = converter.convert(input, template)
        self.assertEqual(test_output, output)
