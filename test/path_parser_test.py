import unittest
import json

from src.path_parser import PathParser
from src.structure_parser.structure_parser import StructureParser
from src.dependency_injector import DependencyInjector
from src.function_parser import FunctionParser
from src.cluster_parser import ClusterParser

input_json = """
{
    "data": {
        "Rows": [
            {
                "Columns": [
                    {
                        "Name": "Product",
                        "Value": "PH-20151231-01"
                    },
                    {
                        "Name": "High",
                        "Value": "14,00"
                    },
                    {
                        "Name": "Low",
                        "Value": "11,30"
                    },
                    {
                        "Name": "Last",
                        "Value": "11,30"
                    },
                    {
                        "Name": "Avg",
                        "Value": "13,33"
                    },
                    {
                        "Name": "Volume",
                        "Value": "40,00"
                    }
                ],
                "Name": "PH-20151231-01"
            },
            {
                "Columns": [
                    {
                        "Name": "Product",
                        "Value": "PH-20151231-02"
                    },
                    {
                        "Name": "High",
                        "Value": "13,00"
                    },
                    {
                        "Name": "Low",
                        "Value": "11,10"
                    },
                    {
                        "Name": "Last",
                        "Value": "13,00"
                    },
                    {
                        "Name": "Avg",
                        "Value": "12,71"
                    },
                    {
                        "Name": "Volume",
                        "Value": "65,00"
                    }
                ],
                "Name": "PH-20151231-02"
            }
        ],
        "DataStartdate": "2014-11-25T00:00:00",
        "DataEnddate": "2016-01-01T08:00:00Z"
    },

    "endDate": "31-12-2015"
}
"""


class PathParserTest(unittest.TestCase):
    dependency_injection = DependencyInjector(PathParser, FunctionParser, StructureParser, {"$cluster": ClusterParser})

    parser = PathParser(dependency_injection)
    input = json.loads(input_json)
    required_output = [["Product", "High", "Low", "Last", "Avg", "Volume"],
                       ["Product", "High", "Low", "Last", "Avg", "Volume"]]
    steps = "data.Rows.Columns.Name"

    def test_parse_path(self):
        path = "_." + self.steps
        test_output = self.parser.parse(path, self.input)
        self.assertEqual(test_output, self.required_output)

    def test_parse_path_flatten(self):
        path = "__." + self.steps
        test_output = self.parser.parse(path, self.input)
        self.assertEqual(test_output, self.required_output)

    def test_minus(self):
        minus_from = "_.step.another.third"
        minus_path = "_.step.another"
        required_output = "_.third"
        test_output = self.parser.minus(minus_from, minus_path)
        self.assertEqual(test_output, required_output)

    def test_minus_first_flatten(self):
        minus_from = "__.step.another.third"
        minus_path = "_.step.another"
        required_output = "__.third"
        test_output = self.parser.minus(minus_from, minus_path)
        self.assertEqual(test_output, required_output)

    def test_minus_second_flatten(self):
        minus_from = "_.step.another.third"
        minus_path = "__.step.another"
        required_output = "_.third"
        test_output = self.parser.minus(minus_from, minus_path)
        self.assertEqual(test_output, required_output)

    def test_minus_on_match(self):
        minus_from = "_.step.another.third"
        minus_path = "_.step.anohteer"
        test_output = self.parser.minus(minus_from, minus_path)
        self.assertEqual(test_output, minus_from)

    def test_minus_more_than_original(self):
        minus_from = "_.step.another"
        minus_path = "_.step.another.third"
        test_output = self.parser.minus(minus_from, minus_path)
        self.assertEqual(test_output, minus_from)
