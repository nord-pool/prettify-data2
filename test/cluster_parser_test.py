# This file contains basically only integration tests.

import unittest
import json

from src.structure_parser.structure_parser import StructureParser
from src.dependency_injector import DependencyInjector
from src.function_parser import FunctionParser
from src.path_parser import PathParser
from src.cluster_parser import ClusterParser


class ClusterParserTest(unittest.TestCase):
    dependency_injector = DependencyInjector(PathParser, FunctionParser, StructureParser, [])
    parser = ClusterParser(dependency_injector)

    def test_parse_path(self):
        input = json.loads("""
        {
            "data": {
                "rows": [
                    {"name": "TestName", "age":"13"},
                    {"name": "TestName2", "age":"16"}
                ]
            }
        }
        """)

        template = json.loads("""
        {
            "_.data.rows": {
                "testname": "_.data.rows.name",
                "testage": "_.data.rows.age"
            }
        }""")

        output = json.loads("""
        [
            {"testname": "TestName","testage": "13"},
            {"testname": "TestName2","testage": "16"}
        ]""")

        test_output = self.parser.parse(template, input)
        self.assertEqual(test_output, output)
