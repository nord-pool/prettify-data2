
"""
Takes JSON and template
initializes parser interface: parser(path_parser, structure_parser, function_parser, available_functions)

This is the class that orchestrates between parsers

"""
import copy


class DependencyInjector(object):

    def __init__(self, path_constructor, function_constructor, structure_constructor, function_constructors):
        self.path_constructor = path_constructor
        self.function_constructor = function_constructor
        self.structure_constructor = structure_constructor
        self.function_constructors = function_constructors

    class LazyService:
        service = None

        def __init__(self, dependency_injector, constructor):
            self.constructor = constructor
            self.dependency_injector = dependency_injector

        def lazy_service(self):
            if self.service is None:
                self.service = self.constructor(self.dependency_injector)
            return self.service

    def lazy_path_service(self):
        return self.LazyService(self, self.path_constructor).lazy_service

    def lazy_function_service(self):
        return self.LazyService(self, self.function_constructor).lazy_service

    def lazy_structure_service(self):
        return self.LazyService(self, self.structure_constructor).lazy_service

    def function_dict(self):
        # todo: no need ot be deepcopy anymore?
        return copy.deepcopy(self.function_constructors)
