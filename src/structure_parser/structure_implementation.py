class StructureImplementation:
    def __init__(self, dependency_injector):
        self.path_service = dependency_injector.lazy_path_service()
        self.function_service = dependency_injector.lazy_function_service()

    def parse(self, data, template):
        if isinstance(template, list):
            return self.parse_list(data, template)

        elif isinstance(template, dict):
            return self.parse_dict(data, template)

        elif isinstance(template, str):
            return self.path_service().parse(data, template)
        else:
            raise Exception("not implemented")

    def flatten_sign(self):
        return "_"

    def is_function(self, name):
        return isinstance(name, str) and name.startswith(self.function_service().function_sign())

    def parse_list(self, data, template):
        parsed_template = []
        for key in template:
            parsed_template.append(self.parse(data, key))

        return parsed_template

    def parse_dict(self, data, template):
        parsed_template = {}
        for key in template:
            value = template[key]
            if self.is_function(value):
                raise Exception("dictionary value cannot be function")
            if self.is_function(key):
                return self.function_service().parse(key, value, data)
            else:
                new_parsed = self.get_parsed_key_value_pair(data, key, value)
                parsed_template = {**new_parsed, **parsed_template}

        return parsed_template

    def get_parsed_key_value_pair(self, data, key, value):
        parsed_key = self.path_service().parse(key, data)
        parsed_value = self.get_parsed_value(value, data)
        return self.flatten(key, parsed_key, value, parsed_value)

    def get_parsed_value(self, value, data):
        if isinstance(value, list):
            return self.parse_list(data, value)
        elif isinstance(value, dict):
            return self.parse_dict(data, value)
        else:
            return self.path_service().parse(value, data)

    def flatten(self, key, key_parsed, value, value_parsed):
        if self.is_flatten(key) and self.is_flatten(value):
            return dict(zip(key_parsed, value_parsed))
        elif self.is_flatten(key):
            return self.combine_when_flatten_key(key_parsed, value_parsed)
        elif isinstance(key_parsed, list):
            raise Exception("JSON key value cannot be list. key list is ", key_parsed)
        elif self.is_flatten(value):
            return self.combine_when_flatten_value(key_parsed, value_parsed)
        else:
            return {key_parsed: value_parsed}

    def is_flatten(self, path):
        if isinstance(path, str):
            flatten_starts_with = self.flatten_sign() + \
                                  self.path_service().path_sign() + \
                                  self.path_service().step_separator()
            return path.startswith(flatten_starts_with)
        else:
            return False

    @staticmethod
    def combine_when_flatten_key(key_parsed, value_parsed):
        if isinstance(key_parsed, list):
            return {key_parsed[0]: value_parsed}
        else:
            return {key_parsed: value_parsed}

    @staticmethod
    def combine_when_flatten_value(key_parsed, value_parsed):
        if isinstance(value_parsed, list):
            return {key_parsed: value_parsed[0]}
        else:
            return {key_parsed: value_parsed}
