"""
Public API
"""

from src.structure_parser.structure_implementation import StructureImplementation


class StructureParser:
    def __init__(self, dependency_injector):
        # this could be injected
        self.functions = StructureImplementation(dependency_injector)

    def parse(self, data, template):
        return self.functions.parse(data, template)

    def flatten_sign(self):
        return self.functions.flatten_sign()

    def flatten(self, key, key_parsed, value, value_parsed):
        return self.functions.flatten(key, key_parsed, value, value_parsed)
