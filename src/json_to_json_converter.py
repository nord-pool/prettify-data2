from src.path_parser import PathParser
from src.structure_parser.structure_parser import StructureParser
from src.dependency_injector import DependencyInjector
from src.function_parser import FunctionParser
from src.cluster_parser import ClusterParser


class JsonToJsonConverter:
    dependency_injection = DependencyInjector(PathParser, FunctionParser, StructureParser, {"$cluster": ClusterParser})

    def convert(self, json_to_convert, json_template):
        return self.dependency_injection.lazy_structure_service()().parse(json_to_convert, json_template)


