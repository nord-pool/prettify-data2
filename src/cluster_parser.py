"""

"""

# todo: separate public api and implementation
# todo: should cluster deep.
class ClusterParser:
    def __init__(self, dependency_injector):
        self.path_service = dependency_injector.lazy_path_service()
        self.structure_service = dependency_injector.lazy_structure_service()

    def parse(self, template, data):
        clustered_list = []
        for cluster_by_path in template:
            clustered_block = self.cluster_block(data, template, cluster_by_path)
            clustered_list.append(clustered_block)
        if len(clustered_list) == 1:
            return clustered_list[0]
        else:
            return clustered_list

    def cluster_block(self, data, template, cluster_by_path):
        clustered = []
        cluster_by_objects = self.path_service().parse(cluster_by_path, data)
        clustering_template = template[cluster_by_path]
        for cluster_by_obj in cluster_by_objects:
            clustered.append(self.create_clustered_object(cluster_by_obj, clustering_template, cluster_by_path, data))
        return clustered

    def create_clustered_object(self, cluster_by_obj, clustering_template, cluster_by_path, data):
        clustered_object = {}
        for key in clustering_template:
            value = clustering_template[key]
            new_parsed = self.cluster_key_value_pair(key, value, cluster_by_obj, cluster_by_path, data)
            clustered_object = {**new_parsed, **clustered_object}
        return clustered_object

    # too many parameters. How to descrease the number
    def cluster_key_value_pair(self, key, value, cluster_by_obj, cluster_by_path, data):
        def is_path(path):
            return isinstance(path, str) and self.path_service().is_path(path)

        def cluster_path(path):
            clustered_path = self.path_service().minus(path, cluster_by_path)
            return self.path_service().parse(clustered_path, cluster_by_obj)

        def cluster_key(clustering_target_key):
            if is_path(clustering_target_key):
                return cluster_path(clustering_target_key)
            else:
                return clustering_target_key

        def cluster_value(clustering_target_value):
            if is_path(clustering_target_value):
                return cluster_path(clustering_target_value)
            else:
                return self.structure_service().parse(self, data, clustering_target_value)

        clustered_key = cluster_key(key)
        clustered_value = cluster_value(value)
        return self.structure_service().flatten(key, clustered_key, value, clustered_value)



