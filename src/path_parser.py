#todo: public API
# parse()
# path can start either "_." or "__."
# is_path()

# todo: separate public api and implementation
class PathParser:
    def __init__(self, dependency_injector):
        self.structure_service = dependency_injector.lazy_structure_service()

    def parse(self, path, data):
        if self.is_path(path):
            steps = path.split(self.step_separator())
            return self.parse_steps(steps[1:], data)
        else:
            return path

    def path_sign(self):
        return "_"

    def step_separator(self):
        return "."

    def is_path(self, path):
        def allowed_path_begins():
            path_start_sign = self.path_sign() + self.step_separator()
            return [path_start_sign, self.structure_service().flatten_sign() + path_start_sign]

        if isinstance(path, str):
            return any(path.startswith(path_begin) for path_begin in allowed_path_begins())
        else:
            return False

    def minus(self, minus_from, minus_path):
        def remove_functionality_sings(path):
            return path[path.index(self.step_separator()):]

        if self.is_path(minus_from) is False or self.is_path(minus_path) is False:
            return minus_from
        else:
            steps1 = remove_functionality_sings(minus_from)
            steps2 = remove_functionality_sings(minus_path)

            if steps1.startswith(steps2):
                leading_functionality_signs = minus_from[0:minus_from.index(self.step_separator())]
                subtracted_path = steps1[len(steps2):]
                return leading_functionality_signs + subtracted_path
            else:
                return minus_from

    def parse_steps(self, steps, data):
        if len(steps) == 0:
            return data

        if steps[0] not in data:
            return None
        new_data = data[steps[0]]

        if isinstance(new_data, list):
            mapped_list = []
            for cell in new_data:
                new_cell = self.parse_steps(steps[1:], cell)
                if new_cell is not None:
                    mapped_list.append(new_cell)

            return mapped_list
        else:
            return self.parse_steps(steps[1:], new_data)
