"""
if there is function call in template then this method is called.

Interface:
$function(JSON_scope, data, parser(path_parser, structure_parser, function_parser))

output: parser JSON

"""

# todo: separate public api and implementation
class FunctionParser:
    def __init__(self, dependency_injector):
        self.function_dict = dependency_injector.function_dict()
        self.dependency_injector = dependency_injector

    def parse(self, function_name, template, data):
        function = self.function_dict[function_name](self.dependency_injector)
        return function.parse(template, data)

    def function_sign(self):
        return "$"
